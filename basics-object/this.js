
// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Создайте объект calculator (калькулятор) с тремя методами:
//
// read() (читать) запрашивает два значения и сохраняет их как свойства объекта.
// sum() (суммировать) возвращает сумму сохранённых значений.
// mul() (умножить) перемножает сохранённые значения и возвращает результат.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let calculator = {
    read (num1,num2) {
        this.num1 = num1
        this.num2 = num2
    },
    sum () {
        return this.num1 + this.num2
    },
    mul() {
        return this.num1 * this.num2
    }
}

// calculator.read(10,20)
// console.log(calculator.sum())
// console.log(calculator.mul())

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Это ladder (лестница) – объект, который позволяет подниматься вверх и спускаться:

// let ladder = {
//     step: 0,
//     up() {
//         this.step++;
//     },
//     down() {
//         this.step--;
//     },
//     showStep: function() { // показывает текущую ступеньку
//         alert( this.step );
//     }
// };

// Измените код методов up, down и showStep таким образом, чтобы их вызов можно было сделать по цепочке, например так:

// ladder.up().up().down().showStep(); // 1

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let ladder = {
    step: 0,
    up() {
        this.step++;
        return this
    },
    down() {
        this.step--;
        return this
    },
    showStep: function() {
        console.log(this.step);
    }
};


ladder.up().up().up().up().showStep()


