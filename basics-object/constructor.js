
// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Создайте функцию-конструктор Calculator, который создаёт объекты с тремя методами:
//
// read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта.
// sum() возвращает сумму введённых свойств.
// mul() возвращает произведение введённых свойств.

// let calculator = new Calculator();
// calculator.read();
//
// alert( "Sum=" + calculator.sum() );
// alert( "Mul=" + calculator.mul() );


// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

function Calculator() {
    this.read = function () {
        this.num1 = +prompt('Введите превое число')
        this.num2 = +prompt('Введите второе число')
        return this
    }
    this.sum = function () {
        return this.num1 + this.num2
    }
    this.mul = function () {
        return this.num1 * this.num2
    }
}

let calculator = new Calculator()

// calculator.read()

// alert( "Sum=" + calculator.sum() );
// alert( "Mul=" + calculator.mul() );

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию-конструктор Accumulator(startingValue).
//     Объект, который она создаёт, должен уметь следующее:
//     Хранить «текущее значение» в свойстве value. Начальное значение устанавливается в аргументе конструктора startingValue.
//     Метод read() использует prompt для получения числа и прибавляет его к свойству value.
//     Таким образом, свойство value является текущей суммой всего, что ввёл пользователь при вызовах метода read(), с учётом начального значения startingValue.
//     Ниже вы можете посмотреть работу кода:

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

function Accumulator(startingValue) {
    this.value = startingValue
    this.read = function () {
        this.value += +prompt('Введите число')
    }

}

let accumulator = new Accumulator(10); // начальное значение 1

// accumulator.read();
// accumulator.read();
// accumulator.read();


// alert(accumulator.value);



