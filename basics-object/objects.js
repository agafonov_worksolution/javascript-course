// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите код, выполнив задание из каждого пункта отдельной строкой:
//
//     Создайте пустой объект user.
//     Добавьте свойство name со значением John.
//     Добавьте свойство surname со значением Smith.
//     Измените значение свойства name на Pete.
//     Удалите свойство name из объекта.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const user = {}
user.name = 'John'
user.surname = 'Smith'
user.name = 'Pete'
delete user.name

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.

// let schedule = {};
//
// alert( isEmpty(schedule) ); // true
//
// schedule["8:30"] = "get up";
//
// alert( isEmpty(schedule) ); // false

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const isEmpty = (obj) => {
    for (const objKey in obj) {
        return false
    }
    return true
}

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// У нас есть объект, в котором хранятся зарплаты нашей команды:

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
}

const calcSalaries = (obj) => {
    let sum = 0
    for (let objKey in obj) {
        sum += obj[objKey]
    }
    return sum
}

// console.log(calcSalaries(salaries))

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Создайте функцию multiplyNumeric(obj), которая умножает все числовые свойства объекта obj на 2.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const multiplyNumeric = (obj) => {
    for (let objKey in obj) {
        if(typeof obj[objKey] === 'number') {
            obj[objKey] *= 2
        }
    }
}

// до вызова функции
let menu = {
    width: 200,
    height: 300,
    title: "My menu"
};

multiplyNumeric(menu)
// console.log(menu)




























