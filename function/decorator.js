// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Создайте декоратор spy(func), который должен возвращать обёртку, которая сохраняет все вызовы функции в своём свойстве calls.
// Каждый вызов должен сохраняться как массив аргументов.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let work = (a, b) => a + b

const spy = (func) => {
    function wrapper(...args) {
        wrapper.calls.push(args);
        return func.apply(this, arguments);
    }
    wrapper.calls = [];
    return wrapper;
}

work = spy(work);

work(1, 2)
work(3, 4)

// for (let args of work.calls) {
//     console.log( 'call:' + args.join() )
// }

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Создайте декоратор delay(f, ms), который задерживает каждый вызов f на ms миллисекунд.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let log = (x) => console.log(x)

const delay = (func, time) => {
    return function (text) {
        setTimeout(() => func.apply(this, arguments), time)
    }
}

let f1500 = delay(log, 1500);
let f1000 = delay(log, 2500);

// f1500("test");
// f1000("task")



















