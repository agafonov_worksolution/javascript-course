// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию sum, которая работает таким образом: sum(a)(b) = a+b.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const sum = (a) => {
    return function (b) {
        return a + b
    }
}

// console.log(sum(5)(-1))

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// У нас есть встроенный метод arr.filter(f) для массивов. Он фильтрует все элементы с помощью функции f.
//     Если она возвращает true, то элемент добавится в возвращаемый массив.
//
//     Сделайте набор «готовых к употреблению» фильтров:
//
//     inBetween(a, b) – между a и b (включительно).
// inArray([...]) – находится в данном массиве.
//     Они должны использоваться таким образом:
//
//     arr.filter(inBetween(3,6)) – выбирает только значения между 3 и 6 (включительно).
// arr.filter(inArray([1,2,3])) – выбирает только элементы, совпадающие с одним из элементов массива

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let arr = [1, 2, 3, 4, 5, 6, 7];

const inBetween = (a, b) => {
    return  (el) => (el >= a && el <= b)
}

const inArray = (arr) => {
    return (el) => arr.includes(el)
}

// console.log(arr.filter(inBetween(3,6)))
// console.log(arr.filter(inArray([1,2,10])))

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// У нас есть массив объектов, который нужно отсортировать:
//
//     let users = [
//         { name: "John", age: 20, surname: "Johnson" },
//         { name: "Pete", age: 18, surname: "Peterson" },
//         { name: "Ann", age: 19, surname: "Hathaway" }
//     ];
// Обычный способ был бы таким:
//
// // по имени (Ann, John, Pete)
//     users.sort((a, b) => a.name > b.name ? 1 : -1);
//
// // по возрасту (Pete, Ann, John)
// users.sort((a, b) => a.age > b.age ? 1 : -1);
// Можем ли мы сделать его короче, скажем, вот таким?
//
//     users.sort(byField('name'));
// users.sort(byField('age'));
// То есть, чтобы вместо функции, мы просто писали byField(fieldName).
//
//     Напишите функцию byField, которая может быть использована для этого.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let users = [
    { name: "John", age: 20, surname: "Johnson" },
    { name: "Pete", age: 18, surname: "Peterson" },
    { name: "Ann", age: 19, surname: "Hathaway" }
];

const byField = (fieldName) => {
    return (a,b) => a[fieldName] > b[fieldName] ? 1 : -1
}


// users.sort(byField('name'));
// users.forEach(user => console.log(user.name)); // Ann, John, Pete
//
// users.sort(byField('age'));
// users.forEach(user => console.log(user.name)); // Pete, Ann, John






















