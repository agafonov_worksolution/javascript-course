// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию printNumbers(from, to), которая выводит число каждую секунду, начиная от from и заканчивая to.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const printNumbers = (from, to) => {
    let tick = from
    let tickInterval = setInterval(() => {
        console.log(tick)
        if (tick === to) {
            clearInterval(tickInterval)
        }
        tick++
    },1000)
}

// printNumbers(1,10)
