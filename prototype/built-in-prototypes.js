// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Добавьте всем функциям в прототип метод defer(ms), который вызывает функции через ms миллисекунд.

// function f() {
//     alert("Hello!");
// }
//
// f.defer(1000); // выведет "Hello!" через 1 секунду

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const log = () => {
    console.log("Hello!");
}

Function.prototype.deferOne = function (ms) {
    setTimeout(this, ms)
}

// log.deferOne(1000)

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Добавьте всем функциям в прототип метод defer(ms), который возвращает обёртку, откладывающую вызов функции на ms миллисекунд.

// function f(a, b) {
//     alert( a + b );
// }
//
// f.defer(1000)(1, 2); // выведет 3 через 1 секунду.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const f = (a, b) => console.log( a + b )

Function.prototype.deferTwo = function (ms) {
    let upThis = this
    return function (...args) {
        setTimeout(() => upThis.apply(this, args),ms)
    }
}

// f.deferTwo(3000)(1, 2)






