// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// У нас есть объект:

// let user = {
//     name: "John",
//     years: 30
// };

// Напишите деструктурирующее присваивание, которое:
//
// свойство name присвоит в переменную name.
//     свойство years присвоит в переменную age.
//     свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)


// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let user = {
    name: "John",
    years: 30
};

let {name, years, isAdmin = false} = user

// console.log(name)
// console.log(years)
// console.log(isAdmin)

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Создайте функцию topSalary(salaries), которая возвращает имя самого высокооплачиваемого сотрудника.
//
//     Если объект salaries пустой, то нужно вернуть null.
//     Если несколько высокооплачиваемых сотрудников, можно вернуть любого из них.
//     P.S. Используйте Object.entries и деструктурирование, чтобы перебрать пары ключ/значение.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250,
    "yorik": 500
};

const topSalary = (salaries) => {
    let max = 0
    let nameM = null
    for (let [name, value] of Object.entries(salaries)) {
        // (value > max) ? [max = value, nameM = name] : null
        if (value > max) {
            max = value
            nameM = name
        }
    }
    return nameM
}

console.log(topSalary(salaries))



