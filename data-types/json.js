// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Преобразуйте user в JSON, затем прочитайте этот JSON в другую переменную.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let user = {
    name: "Василий Иванович",
    age: 35
};

let userJSON = JSON.stringify(user)
let newUser = JSON.parse(userJSON)

// console.log(userJSON)
// console.log(newUser)
