// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Есть объект salaries с произвольным количеством свойств, содержащих заработные платы.
// Напишите функцию sumSalaries(salaries), которая возвращает сумму всех зарплат с помощью метода Object.values и цикла for..of.
// Если объект salaries пуст, то результат должен быть 0.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250
};

const sumSalaries = (salaries) => {
    let sum = 0
    for (let value of Object.values(salaries)) {
        sum += value
    }
    return sum
}

// console.log(sumSalaries(salaries))

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию count(obj), которая возвращает количество свойств объекта:

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let user = {
    name: 'John',
    age: 30
};

let userLength = (obj) => Object.keys(obj).length

console.log(userLength(user))











