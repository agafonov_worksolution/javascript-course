// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» в «myShortString».
// То есть дефисы удаляются, а все слова после них получают заглавную букву.
//
// camelize("background-color") == 'backgroundColor';
// camelize("list-style-image") == 'listStyleImage';
// camelize("-webkit-transition") == 'WebkitTransition';

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const camelize = (str) => str.split('-').map((el, i) => i === 0 ? el: el[0].toUpperCase() + el.slice(1)).join('')

// console.log(camelize("-background-color"))

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет в нём элементы между a и b и отдаёт массив этих элементов.
// Функция должна возвращать новый массив и не изменять исходный.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let arr = [5, 3, 8, 1, 2, 9, 3, 7, 8, 12, 12];

const filterRange = (arr, a, b) => arr.filter(el => (el >= a && el <= b))

// console.log(filterRange(arr, 1,10))

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию filterRangeInPlace(arr, a, b), которая принимает массив arr и удаляет из него все значения кроме тех,
// которые находятся между a и b. То есть, проверка имеет вид a ≤ arr[i] ≤ b.
// Функция должна изменять принимаемый массив и ничего не возвращать.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const filterRangeInPlace = (arr, a, b) => {
    for (let i = 0; i < arr.length ; i++) {
        if(!(arr[i] >= a && arr[i] <= b)) {
            arr.splice(i,1)
            i--
        }
    }
}

// filterRangeInPlace(arr, 1,4)
// console.log(arr)

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// У нас есть массив строк arr. Нужно получить отсортированную копию, но оставить arr неизменённым.
// Создайте функцию copySorted(arr), которая будет возвращать такую копию.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++
let arr1 = ["Python", "JavaScript", "CSS", "HTML" ]

const copySorted = (arr) => arr.slice().sort()

// let sorted = copySorted(arr1)
// console.log(sorted)

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Сортировать в порядке по убыванию

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let arr2 = [5, 2, 1, -10, 8];

arr2.sort((a,b) => b - a)

// console.log(arr2)

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// У вас есть массив объектов user, и в каждом из них есть user.name. Напишите код, который преобразует их в массив имён.

// let vasya = { name: "Вася", age: 25 };
// let petya = { name: "Петя", age: 30 };
// let masha = { name: "Маша", age: 28 };
//
// let users = [ vasya, petya, masha ];
//
// let names = /* ... ваш код */
//
//     alert( names ); // Вася, Петя, Маша

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

let vasya = { name: "Вася", surname: "Пупкин", id: 1, age: 25};
let petya = { name: "Петя", surname: "Иванов", id: 2, age: 30};
let masha = { name: "Маша", surname: "Петрова", id: 3, age: 29};

let users = [ vasya, petya, masha ];

let name = users.map((el) => el.name)

// console.log(name)

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// У вас есть массив объектов user, и у каждого из объектов есть name, surname и id.
// Напишите код, который создаст ещё один массив объектов с параметрами id и fullName, где fullName – состоит из name и surname.

/*
usersMapped = [
  { fullName: "Вася Пупкин", id: 1 },
  { fullName: "Петя Иванов", id: 2 },
  { fullName: "Маша Петрова", id: 3 }
]
*/

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++


let usersMapped = users.map((el) => ({fullName: `${el.name} ${el.surname}`, id: el.id}))

// console.log(usersMapped[0].id)
// console.log(usersMapped[0].fullName)

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию sortByAge(users), которая принимает массив объектов со свойством age и сортирует их по нему.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const sortByAge = (arr) => arr.sort((a,b) => a.age - b.age)


sortByAge(users)

// console.log(users[0].name); // Вася
// console.log(users[1].name); // Маша
// console.log(users[2].name); // Петя

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию getAverageAge(users), которая принимает массив объектов со свойством age и возвращает средний возраст.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const getAverageAge = (arr) => arr.reduce((acc,el) => acc + el.age, 0) / arr.length


// console.log(getAverageAge(users))





