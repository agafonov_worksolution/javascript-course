// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Следующая функция возвращает true, если параметр age больше 18.
// В ином случае она задаёт вопрос confirm и возвращает его результат.

// function checkAge(age) {
//     if (age > 18) {
//         return true;
//     } else {
//         return confirm('Родители разрешили?');
//     }
// }

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

function checkAge(age) {
    return (age > 18) ? true: confirm('Родители разрешили?')
}

function checkAge1(age) {
    return (age > 18) || confirm('Родители разрешили?')
}

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию min(a,b), которая возвращает меньшее из чисел a и b.

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

function getMin(a,b) {
    return a > b ? b: a
}


console.log(getMin(5,9))
console.log(getMin(11,7))

// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Напишите функцию pow(x,n), которая возвращает x в степени n. Иначе говоря, умножает x на себя n раз и возвращает результат.

function getPow(x,n) {
    return x ** n
}

console.log(getPow(3,2))
console.log(getPow(3,3))
console.log(getPow(1,100))


















