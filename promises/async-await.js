// ЗАДАЧА++++++++++++++++++++++++++++++++++++++++++++++++++++

// Перепишите один из примеров раздела Цепочка промисов, используя async/await вместо .then/catch:
//
// function loadJson(url) {
//     return fetch(url)
//         .then(response => {
//             if (response.status == 200) {
//                 return response.json();
//             } else {
//                 throw new Error(response.status);
//             }
//         })
// }
//
// loadJson('no-such-user.json') // (3)
//     .catch(alert); // Error: 404

// РЕШЕНИЕ++++++++++++++++++++++++++++++++++++++++++++++++++++

const loadJson = async (url) => {
    let response = await fetch(url)
    response.status === 200 ? await response.json() : null

}

// loadJson('no-such-user.json')